#!/bin/sh

# Repositories
yum localinstall -y --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Update && Upgrade
yum -y update yum
yum -y update

# Install common tools
yum -y install zsh git vim tree htop wget curl most
yum -y install guake texlive texlive-moderncv texmaker
yum -y install gcc clang ctags valgrind
yum -y install vlc libreoffice transmission
yum -y install realcrypt gparted

# Spotify
yum -y install python-devel dbus-x11 redhat-lsb-core lpf-spotify-client
lpf-update

# My conf
sh my_conf.sh
