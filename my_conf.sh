#!/bin/sh

# My conf
git clone https://obitux@bitbucket.org/obitux/vim.git ~/.vim
sh ~/.vim/install.sh
git clone https://obitux@bitbucket.org/obitux/conf.git ~/git/conf
rm -f ~/.gitconfig
ln -s ~/git/conf/.gitconfig ~/.gitconfig
rm -f ~/.zshrc
ln -s ~/git/conf/.zshrc ~/.zshrc
chsh -s /bin/zsh

# For root
