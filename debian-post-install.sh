#!/bin/sh

# Repositories
echo "# Debian Jessie" >> /etc/apt/sources.list
echo "deb http://ftp.fr.debian.org/debian jessie main contrib non-free" > /etc/apt/sources.list
echo "deb http://ftp.debian.org/debian/ jessie-updates main contrib non-free" >> /etc/apt/sources.list
echo "deb http://security.debian.org/ jessie/updates main contrib non-free" >> /etc/apt/sources.list
echo "deb http://debian.mirrors.ovh.net/debian/ jessie-backports main" >> /etc/apt/sources.list

# Update && Upgrade
apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get -y autoremove

# Install common tools
apt-get -y install zsh git vim tree htop wget curl most
apt-get -y install guake texmaker
apt-get -y install build-essential gcc clang exuberant-ctags valgrind
apt-get -y install vlc libreoffice
apt-get -y install tor vidalia

# Firefox
echo "# Firefox" >> /etc/apt/sources.list
echo "deb http://cdn.debian.net/debian unstable main" >> /etc/apt/sources.list
echo "deb http://cdn.debian.net/debian experimental main" >> /etc/apt/sources.list
apt-get update
apt-get -y install -t experimental iceweasel

# Dropbox
#wget https://www.dropbox.com/download?dl=packages/debian/dropbox_1.6.0_amd64.deb
#dpkg -i dropbox_1.6.0_amd64.deb
#rm dropbox_1.6.0_amd64.deb

# Spotify
echo "# Spotify" >> /etc/apt/sources.list
echo "deb http://repository.spotify.com stable non-free" >> /etc/apt/sources.list
wget http://ftp.fr.debian.org/debian/pool/main/o/openssl/libssl0.9.8_0.9.8o-4squeeze14_amd64.deb
sudo dpkg -y -i libssl0.9.8_0.9.8o-4squeeze14_amd64.deb
rm libssl0.9.8_0.9.8o-4squeeze14_amd64.deb
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 94558F59
apt-get update
apt-get -y upgrade
apt-get -y install spotify-client

# Truecrypt
wget http://www.truecrypt.org/download/truecrypt-7.1a-linux-console-x64.tar.gz
tar -zxvf truecrypt-7.1a-linux-console-x64.tar.gz
rm truecrypt-7.1a-linux-console-x64.tar.gz
./truecrypt-7.1a-linux-console-x64
rm truecrypt-7.1a-linux-console-x64

# My conf
sh my_conf.sh

# KDE
#apt-get -y install kde-full kde-config-touchpad kde-config-gtk-style
#apt-get -y install transmission-qt
