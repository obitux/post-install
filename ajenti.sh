#!/bin/sh

# Ajenti
wget http://repo.ajenti.org/debian/key -O- | apt-key add -
sudo echo "deb http://repo.ajenti.org/debian main main debian" >> /etc/apt/sources.list
sudo apt-get update && apt-get -y install ajenti
sudo service ajenti restart
