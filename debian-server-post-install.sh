#!/bin/sh

# Update && Upgrade
apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get -y autoremove

# Install common tools
apt-get -y install zsh git vim tree htop wget curl most
apt-get -y install build-essential gcc clang exuberant-ctags valgrind

sh my_conf.sh
sh ajenti.sh
